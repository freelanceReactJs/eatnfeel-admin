export const reviews = () => {
  return fetch('http://localhost:3000/review', {
    method: "get"
  })
    .then(response => response.json())
    .catch(error => console.log(error));
}

export const deleteReview = (reviewId) => {
  return fetch('http://localhost:3000/review/' + reviewId, {
    method: "delete",
    headers: {
      "authorization": "Bearer " + localStorage.getItem("adminToken")
    }
  }).then(response => response.json())
    .catch(error => console.log(error));
}