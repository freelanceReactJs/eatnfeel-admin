export const adminRegister = (admin) => {
  return fetch("http://localhost:3000/super/signup", {
    method: "post",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(admin)
  })
    .then(response => response.json())
    .catch(error => console.log(error));
}

export const adminLogin = (login) => {
  return fetch("http://localhost:3000/super/signin", {
    method: "post",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(login)
  })
    .then(response => response.json())
    .catch(error => console.log(error));
}