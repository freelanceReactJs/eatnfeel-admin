export const menu = () => {
  return fetch('http://localhost:3000/menu', {
    method: "get"
  })
    .then(response => response.json())
    .catch(error => console.log(error));
}

export const updateMenu = (Data, menuId) => {
  return fetch('http://localhost:3000/menu/' + menuId, {
    method: "put",
    headers: {
      "authorization": "Bearer " + localStorage.getItem("adminToken")
    },
    body: Data
  })
    .then(response => response.json())
    .catch(error => console.log(error));
}

export const updateMenuImage = (data, menuId) => {
  return fetch('http://localhost:3000/menu/image/' + menuId, {
    method: "put",
    headers: {
      "authorization": "Bearer " + localStorage.getItem("adminToken")
    },
    body: data
  }).then(response => response.json())
    .catch(error => console.log(error));
}

export const deleteMenu = (menuId) => {
  return fetch('http://localhost:3000/menu/' + menuId, {
    method: "delete",
    headers: {
      "authorization": "Bearer " + localStorage.getItem("adminToken")
    }
  }).then(response => response.json())
    .catch(error => console.log(error));
}

export const addMenu = (Data) => {
  return fetch('http://localhost:3000/menu/add', {
    method: "post",
    headers: {
      "authorization": "Bearer " + localStorage.getItem("adminToken")
    },
    body: Data
  })
    .then(response => response.json())
    .catch(error => console.log(error));
}