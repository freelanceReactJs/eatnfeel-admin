export const table = () => {
  return fetch('http://localhost:3000/booking', {
    method: "get"
  })
    .then(response => response.json())
    .catch(error => console.log(error));
}

export const deleteTable = (TableId) => {
  return fetch('http://localhost:3000/book/' + TableId, {
    method: "delete",
    headers: {
      // "authorization": "Bearer " + localStorage.getItem("adminToken")
      "content-type": "application/json"
    }
  })
    .then(response => response.json())
    .catch(error => console.log(error));
}

export const cancleTable = () => {
  return fetch('http://localhost:3000/booking/cancel', {
    method: "get"
  })
    .then(response => response.json())
    .catch(error => console.log(error));
}

export const bookedTable = () => {
  return fetch('http://localhost:3000/booking/booked', {
    method: "get"
  })
    .then(response => response.json())
    .catch(error => console.log(error));
}

export const todaysBooking = () => {
  return fetch('http://localhost:3000/booking/today', {
    method: "get"
  })
    .then(response => response.json())
    .catch(error => console.log(error));
}