import React from 'react';
import { adminRegister, adminLogin } from '../auth/admin'
import { menu, updateMenu, updateMenuImage, deleteMenu, addMenu } from '../auth/menu';
import { table, deleteTable, cancleTable, bookedTable, todaysBooking } from '../auth/table';
import { reviews, deleteReview } from '../auth/review';
import _ from 'lodash';
import 'react-notifications/lib/notifications.css';
import { NotificationContainer, NotificationManager } from 'react-notifications';



const AppContext = React.createContext();

class AppProvider extends React.Component {
  state = {
    switches: true,
    name: "",
    email: "",
    password: "",
    authKey: "",
    lemail: "",
    lpassword: "",
    menus: [],
    category: [],
    menuViewModal: false,
    menuImgModal: false,
    menuViews: [],
    imageView: [],
    itemName: "",
    itemDesc: "",
    itemType: "",
    itemLabel: "",
    itemPrice: "",
    itemImg: "",
    itemNameIns: "",
    itemDescIns: "",
    itemTypeIns: "",
    itemLabelIns: "",
    itemPriceIns: "",
    itemImgIns: "",
    menuDelete: [],
    menuDeleteModal: false,
    tables: [],
    cancelTables: [],
    tableDelete: [],
    bookedTables: [],
    todaysTables: [],
    tableDeleteModal: false,
    reviews: [],
    reviewDeleteModal: false,
    reviewDelete: []
  }

  //lifecycle methods
  componentDidMount() {
    this.fetchMenu();
    this.fetchTableBooking();
    this.getCancelledTableBooking();
    this.getBookedTables();
    this.getTodaysTables();
    this.getAllReviews();
  }

  //login and logout methods begins
  swtichLogin = () => {
    this.setState({ switches: !this.state.switches });
  }

  getName = (event) => {
    this.setState({ name: event.target.value });
  }

  getEmail = (event) => {
    this.setState({ email: event.target.value });
  }

  getPassword = (event) => {
    this.setState({ password: event.target.value });
  }

  getKey = (event) => {
    this.setState({ authKey: event.target.value });
  }

  getLemail = (event) => {
    this.setState({ lemail: event.target.value });
  }

  getLpassword = (event) => {
    this.setState({ lpassword: event.target.value });
  }

  adminRegister = () => {
    const { name, email, password, authKey } = this.state;
    const admin = {
      name, email, password, authKey
    }
    adminRegister(admin)
      .then(data => {
        if (data.status === "success") {
          NotificationManager.success("Registered successfully! Please login");
          this.setState({ name: "", email: "", password: "", authKey: "" });
          this.swtichLogin();
        }
        else {
          NotificationManager.error(`${data.error}`);
        }
      })
      .catch(console.log);
  }

  onLogout = () => {
    localStorage.clear();
    this.toReload();
  }

  adminLogin = () => {
    const { lemail, lpassword } = this.state;
    const login = {
      email: lemail,
      password: lpassword
    }
    adminLogin(login)
      .then(data => {
        if (data.status === "success") {
          localStorage.setItem("adminLogin", true);
          localStorage.setItem("admin", data.result[0].name);
          localStorage.setItem("adminEmail", data.result[0].email);
          localStorage.setItem("adminToken", data.token);
          this.setState({ lemail: "", lpassword: "" });
          this.toReload();
          NotificationManager.success(`Welcome ${data.result[0].name} you logged in successfully`);
        } else {
          NotificationManager.error(`${data.error}`);
        }
      })
  }

  toReload = () => {
    setTimeout(function () {
      window.location.reload()
    }, 500);
  }
  //login and logout methods ends here

  /* Menu related methods begins*/

  fetchMenu = () => {
    menu().then(data => {
      this.setState({ menus: data, category: _.uniq(data.map((data) => { return data.labels })) });
    }).catch(console.log);
  }

  handleViewModal = (menuId) => {
    this.setState(
      {
        menuViews: this.state.menus.filter(x => x._id === menuId),
        menuViewModal: true
      });
  }

  closeViewModal = () => {
    this.setState({ menuViewModal: false })
  }

  handleImageModal = (menuId) => {
    this.setState(
      {
        imageView: this.state.menus.filter(x => x._id === menuId),
        menuImgModal: true
      }
    )
  }

  closeImageModal = () => {
    this.setState({ menuImgModal: false })
  }

  handleDeleteModal = (menuId) => {
    this.setState(
      {
        menuDelete: this.state.menus.filter(x => x._id === menuId),
        menuDeleteModal: true
      }
    )
  }

  closeDeleteModal = () => {
    this.setState({ menuDeleteModal: false })
  }

  getMenuName = (event) => {
    this.setState({ itemName: event.target.value });
  }

  getMenuDesc = (event) => {
    this.setState({ itemDesc: event.target.value });
  }

  getMenuType = (event) => {
    this.setState({ itemType: event.target.value });
  }

  getMenuLabel = (event) => {
    this.setState({ itemLabel: event.target.value });
  }

  getMenuPrice = (event) => {
    this.setState({ itemPrice: event.target.value });
  }

  getMenuImage = (event) => {
    this.setState({ itemImg: event.target.files[0] })
  }


  getMenuNameIns = (event) => {
    this.setState({ itemNameIns: event.target.value });
  }

  getMenuDescIns = (event) => {
    this.setState({ itemDescIns: event.target.value });
  }

  getMenuTypeIns = (event) => {
    this.setState({ itemTypeIns: event.target.value });
  }

  getMenuLabelIns = (event) => {
    this.setState({ itemLabelIns: event.target.value });
  }

  getMenuPriceIns = (event) => {
    this.setState({ itemPriceIns: event.target.value });
  }

  getMenuImageIns = (event) => {
    this.setState({ itemImgIns: event.target.files[0] })
  }

  updateMenus = (mid) => {
    const { itemName, itemDesc, itemLabel, itemType, itemPrice } = this.state;
    const formData = new FormData();
    formData.append('name', itemName);
    formData.append('description', itemDesc);
    formData.append('labels', itemLabel);
    formData.append('price', itemPrice);
    formData.append('food_type', itemType);
    updateMenu(formData, mid)
      .then(data => {
        if (data.status === "success") {
          NotificationManager.success("Item Updated Successfully!...");
          this.fetchMenu();
          this.closeViewModal();
        } else {
          NotificationManager.error(`${data.error}`);
        }
      })
      .catch(console.log);
  }

  updateMenusImage = (mid) => {
    const formData = new FormData();
    formData.append('imgPath', this.state.itemImg);
    updateMenuImage(formData, mid)
      .then(data => {
        if (data.status === "success") {
          NotificationManager.success("Item Image Updated Successfully!...");
          this.fetchMenu();
          this.closeImageModal();
        } else {
          NotificationManager.error(`${data.error}`);
        }
      })
      .catch(console.log);
  }

  deleteMenus = (mid) => {
    deleteMenu(mid)
      .then(data => {
        if (data.status === "success") {
          NotificationManager.success("Menu item deleted successfully!...");
          this.fetchMenu();
          this.closeDeleteModal();
        } else {
          NotificationManager.error(`${data.error}`);
        }
      })
      .catch(console.log);
  }

  addMenus = () => {
    const { itemNameIns, itemDescIns, itemLabelIns, itemTypeIns, itemPriceIns } = this.state;
    const formData = new FormData();
    formData.append('name', itemNameIns);
    formData.append('description', itemDescIns);
    formData.append('labels', itemLabelIns);
    formData.append('price', itemPriceIns);
    formData.append('food_type', itemTypeIns);
    formData.append('imgPath', this.state.itemImgIns);
    addMenu(formData)
      .then(data => {
        if (data.status === "success") {
          NotificationManager.success("Item added successfully!...");
          this.fetchMenu();
          this.setState({
            itemNameIns: '', itemDescIns: '', itemLabelIns: '', itemPriceIns: '', itemTypeIns: '', itemImgIns: ''
          })
        } else {
          NotificationManager.error(`${data.error}`);
        }
      })
      .catch(console.log);
  }

  //Tables related functions
  fetchTableBooking = () => {
    table()
      .then(data => {
        this.setState({ tables: data })
      })
      .catch(console.log);
  }

  handleDeleteBooking = (tableId) => {
    this.setState(
      {
        tableDelete: this.state.tables.filter(x => x._id === tableId),
        tableDeleteModal: true
      }
    )
  }

  closeDeleteBooking = () => {
    this.setState({ tableDeleteModal: false });
  }

  deleteTablesBooking = (tid) => {
    deleteTable(tid)
      .then(data => {
        if (data.status === "success") {
          NotificationManager.success("Table booking deleted successfully!...");
          this.closeDeleteBooking();
          this.fetchTableBooking();
        } else {
          NotificationManager.error(`${data.error}`);
        }
      })
      .catch(console.log);
  }

  getCancelledTableBooking = () => {
    cancleTable().then(data => {
      this.setState({ cancelTables: data });
    }).catch(console.log)
  }

  getBookedTables = () => {
    bookedTable().then(data => {
      this.setState({ bookedTables: data })
    }).catch(console.log);
  }

  getTodaysTables = () => {
    todaysBooking().then(data => {
      this.setState({ todaysTables: data })
    }).catch(console.log);
  }

  //reviews related functions
  getAllReviews = () => {
    reviews().then(data => {
      this.setState({ reviews: data })
    }).catch(console.log)
  }

  handleReviewDelete = (reviewId) => {
    this.setState(
      {
        reviewDelete: this.state.reviews.filter(x => x._id === reviewId),
        reviewDeleteModal: true
      }
    )
  }

  closeReviewModal = () => {
    this.setState({ reviewDeleteModal: false });
  }

  deleteReview = (rid) => {
    deleteReview(rid)
      .then(data => {
        if (data.status === "success") {
          NotificationManager.success("Review deleted successfully!...");
          this.closeReviewModal();
          this.getAllReviews();
        } else {
          NotificationManager.error(`${data.error}`);
        }
      })
      .catch(console.log);
  }

  render() {
    return (
      <AppContext.Provider
        value={{
          ...this.state,
          switcher: this.swtichLogin,
          signUp: this.adminRegister,
          getName: this.getName,
          getEmail: this.getEmail,
          getPassword: this.getPassword,
          getKey: this.getKey,
          signIn: this.adminLogin,
          getLemail: this.getLemail,
          getLpassword: this.getLpassword,
          onLogout: this.onLogout,
          closeViewModal: this.closeViewModal,
          handleViewModal: this.handleViewModal,
          getMenuName: this.getMenuName,
          getMenuDesc: this.getMenuDesc,
          getMenuLabel: this.getMenuLabel,
          getMenuType: this.getMenuType,
          getMenuPrice: this.getMenuPrice,
          updateMenus: this.updateMenus,
          updateMenuImage: this.updateMenusImage,
          handleImageModal: this.handleImageModal,
          closeImageModal: this.closeImageModal,
          getMenuImage: this.getMenuImage,
          deleteMenu: this.deleteMenus,
          handleDeleteModal: this.handleDeleteModal,
          closeDeleteModal: this.closeDeleteModal,
          addMenus: this.addMenus,
          getMenuNameIns: this.getMenuNameIns,
          getMenuDescIns: this.getMenuDescIns,
          getMenuLabelIns: this.getMenuLabelIns,
          getMenuTypeIns: this.getMenuTypeIns,
          getMenuPriceIns: this.getMenuPriceIns,
          getMenuImageIns: this.getMenuImageIns,
          handleDeleteBooking: this.handleDeleteBooking,
          closeDeleteBooking: this.closeDeleteBooking,
          deleteTablesBooking: this.deleteTablesBooking,
          deleteReview: this.deleteReview,
          handleReviewDelete: this.handleReviewDelete,
          closeReviewModal: this.closeReviewModal
        }}
      >
        {this.props.children}
        <NotificationContainer />
      </AppContext.Provider>
    )
  }
}

export { AppProvider, AppContext }