import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import { Route, Switch } from 'react-router-dom';
import SignInUp from './components/SignInUp';
import TopNavbar from './components/Navigations/Navbar';
import Dashboard from './components/Navigations/Dashboard';
import ViewMenu from './components/Menus/ViewMenu';
import EditMenu from './components/Menus/EditMenu';
import EditMenuImage from './components/Menus/EditMenuImage'
import DeleteMenu from './components/Menus/DeleteMenu';
import AddMenu from './components/Menus/AddMenu';
import ViewTable from './components/Table/ViewTable';
import DeleteTable from './components/Table/DeleteTable';
import ViewCancelTable from './components/Table/ViewCancelTable';
import ViewBooked from './components/Table/ViewBooked';
import ViewTodays from './components/Table/ViewTodays';
import ViewReviews from './components/Reviews/ViewReview';
import DeleteReview from './components/Reviews/DeleteReview';

function App() {
  return (
    <div className="App">
      {localStorage.getItem("adminLogin") ?
        <>
          <TopNavbar />
          <Switch>
            <Route path="/" exact component={Dashboard} />
            <Route path="/menu" exact component={ViewMenu} />
            <Route path="/menu/add" exact component={AddMenu} />
            <Route path="/table" exact component={ViewTodays} />
            <Route path="/table/cancelled" exact component={ViewCancelTable} />
            <Route path="/table/booked" exact component={ViewBooked} />
            <Route path="/table/all" exact component={ViewTable} />
            <Route path="/reviews" exact component={ViewReviews} />
          </Switch>
        </>
        : <SignInUp />}
      <EditMenu />
      <EditMenuImage />
      <DeleteMenu />
      <DeleteTable />
      <DeleteReview />
    </div>
  );
}

export default App;
