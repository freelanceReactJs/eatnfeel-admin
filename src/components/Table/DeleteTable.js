import React from 'react';
import { AppContext } from '../../context/AppContext';
import { Modal, Button, Form } from 'react-bootstrap';

export default class DeleteTable extends React.Component {
  render() {
    return (
      <AppContext.Consumer>
        {
          value => {
            const {
              tableDeleteModal,
              closeDeleteBooking,
              deleteTablesBooking,
              tableDelete
            } = value
            return (
              <>
                <Modal show={tableDeleteModal} onHide={closeDeleteBooking}
                  // aria-labelledby="contained-modal-title-vcenter"
                  centered
                >
                  <Modal.Header closeButton>
                    <Modal.Title>Delete Table Booking</Modal.Title>
                  </Modal.Header>
                  {tableDelete.map((data, key) => {
                    return (
                      <Modal.Body>
                        <Form key={key}>
                          <Form.Group>
                            <Form.Label>Are you sure you want to delete <span style={{ fontWeight: "bold", textTransform: "uppercase" }}>{data.name}</span> table booking ?</Form.Label>
                          </Form.Group>
                        </Form>
                        <Modal.Footer>
                          <Button variant="secondary" onClick={closeDeleteBooking} size="sm">
                            Exit
                          </Button>
                          <Button variant="danger" onClick={() => deleteTablesBooking(data._id)} size="sm">
                            Delete
                          </Button>
                        </Modal.Footer>
                      </Modal.Body>
                    )
                  })}
                </Modal>
              </>
            )
          }
        }
      </AppContext.Consumer>
    )
  }
}