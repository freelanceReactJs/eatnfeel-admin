import React from 'react';
import { AppContext } from '../../context/AppContext';
import TableBar from './TableBar';
import styled from 'styled-components';
import { FaTrashAlt } from 'react-icons/fa';
import { Card, Row, Col } from 'react-bootstrap';

export default class ViewCancelTable extends React.Component {

  render() {
    return (
      <AppContext.Consumer>
        {
          value => {
            const { cancelTables, handleDeleteBooking } = value;
            return (
              <>
                <TableBar />
                <ViewTableWrapper>
                  <ViewContent>
                    <div>
                      <h1 className="heads">Cancelled Bookings</h1>
                      {cancelTables.length <= 0 ? <div className="no-record">No Records</div> : null}
                      <div className="arrange">
                        {cancelTables.map((data, key) => {
                          return (
                            <Card className='cards' key={key}>
                              <Card.Body>
                                <Card.Title className="titles">Name.: {data.name}</Card.Title>
                                <Row>
                                  <Col sm={4}>
                                    <Card.Title className="span">Seats.: {data.seats}</Card.Title>
                                  </Col>
                                  <Col sm={4}>
                                    <Card.Title className="span">Date.: {data.date_slot}</Card.Title>
                                  </Col>
                                  <Col sm={4}>
                                    <Card.Title className="span">Time.: {data.time_slot}</Card.Title>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col sm={4}>
                                    <Card.Title className="span">Email: {data.email}</Card.Title>
                                  </Col>
                                  <Col sm={4}>
                                    <Card.Title className="span">Mobile: {data.mobile}</Card.Title>
                                  </Col>
                                  <Col sm={4}>
                                    <Card.Title className="span">Status: <strong>{data.status}</strong></Card.Title>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col sm={12} className="icons">
                                    <FaTrashAlt style={{ marginRight: "5px" }} onClick={() => handleDeleteBooking(data._id)} />
                                  </Col>
                                </Row>
                              </Card.Body>
                            </Card>
                          )
                        })}
                      </div>
                    </div>
                  </ViewContent>
                </ViewTableWrapper>
              </>
            )
          }
        }
      </AppContext.Consumer>
    )
  }
}

const ViewTableWrapper = styled.div`
              width:85%;
              position:absolute;
              top:110px;
              right:0;
  .heads{
                  text - align: left;
                text-transform:capitalize;
                font:bold 25px "Times New Roman";
                padding:10px;
                width:80%;
                border-bottom:1px solid rgba(0,0,0,0.05);
              }
              .no-record{
                width:100%;
                text-align:center;
                margin:20px;
              }
  .titles{
                  text - align: left;
                text-transform:capitalize;
                color:#3B3A3A;
                font:bold 16px "Times New Roman";
              }
  .cards{
                  border - radius: 7px;
                width:100%;
                display:flex;
                margin:10px 20px;
              }
  .arrange{
                  display: flex;
                flex-wrap:wrap;
                justify-content:space-between;
              }
  .span{
                  text - align: left;
                font:15px "Times New Roman";
              }
           `
const ViewContent = styled.div`
              width:90%;
              margin:10px auto;
              box-shadow:0 0 2px 2px rgba(0,0,0,0.1);
              border-radius:5px;
              .icons{
                text-align:right;
              }
              .icons:hover{
                cursor:pointer;
              }
`