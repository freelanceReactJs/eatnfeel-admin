import React from 'react';
import { AppContext } from '../../context/AppContext';
import ViewBar from '../Reviews/ViewBar';
import styled from 'styled-components';
import { FaTrashAlt } from 'react-icons/fa';
import { Card, Row, Col } from 'react-bootstrap';
import { IoIosStar, IoIosStarOutline } from 'react-icons/io';

export default class ViewReview extends React.Component {
  render() {
    return (
      <AppContext.Consumer>
        {
          value => {
            const { reviews, handleReviewDelete } = value;
            return (
              <>
                <ViewBar />
                <ViewTableWrapper>
                  <ViewContent>
                    <div>
                      <h1 className="heads">All Reviews</h1>
                      {reviews.length <= 0 ? <div className="no-record">No Records</div> : null}
                      <div className="arrange">
                        {reviews.map((data, key) => {
                          return (
                            <Card className='cards' key={key}>
                              <Card.Body>
                                <Row>
                                  <Col sm={10}>
                                    <Card.Title className="titles">{data.name}</Card.Title>
                                  </Col>
                                  <Col sm={2}>
                                    <Card.Title className="span">
                                      {data.rating === 5 ? <><IoIosStar /> <IoIosStar /> <IoIosStar /> <IoIosStar /> <IoIosStar /></> : null}
                                      {data.rating === 4 ? <><IoIosStar /> <IoIosStar /> <IoIosStar /> <IoIosStar /> <IoIosStarOutline /></> : null}
                                      {data.rating === 3 ? <><IoIosStar /> <IoIosStar /> <IoIosStar /> <IoIosStarOutline /> <IoIosStarOutline /></> : null}
                                      {data.rating === 2 ? <><IoIosStar /> <IoIosStar /> <IoIosStarOutline /> <IoIosStarOutline /> <IoIosStarOutline /></> : null}
                                      {data.rating === 1 ? <><IoIosStar /> <IoIosStarOutline /> <IoIosStarOutline /> <IoIosStarOutline /> <IoIosStarOutline /></> : null}
                                    </Card.Title>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col sm={12}>
                                    <Card.Title className="span">{data.comment}</Card.Title>
                                  </Col>
                                </Row>
                                {/* <Row>
                                  <Col sm={12} className="icons">
                                    <FaTrashAlt style={{ marginRight: "5px" }} onClick={() => handleReviewDelete(data._id)} />
                                  </Col>
                                </Row> */}
                              </Card.Body>
                            </Card>
                          )
                        })}
                      </div>
                    </div>
                  </ViewContent>
                </ViewTableWrapper>
              </>
            )
          }
        }
      </AppContext.Consumer>
    )
  }
}

const ViewTableWrapper = styled.div`
              width:85%;
              position:absolute;
              top:110px;
              right:0;
  .heads{
                  text - align: left;
                text-transform:capitalize;
                font:bold 25px "Times New Roman";
                padding:10px;
                width:80%;
                border-bottom:1px solid rgba(0,0,0,0.05);
              }
              .no-record{
                width:100%;
                text-align:center;
                margin:20px;
              }
  .titles{
                  text - align: left;
                text-transform:capitalize;
                color:#3B3A3A;
                font:bold 16px "Times New Roman";
              }
  .cards{
                  border - radius: 7px;
                width:100%;
                display:flex;
                margin:10px 20px;
              }
  .arrange{
                  display: flex;
                flex-wrap:wrap;
                justify-content:space-between;
              }
  .span{
                  text - align: left;
                font:15px "Times New Roman";
              }
           `
const ViewContent = styled.div`
              width:90%;
              margin:10px auto;
              box-shadow:0 0 2px 2px rgba(0,0,0,0.1);
              border-radius:5px;
              .icons{
                text-align:right;
              }
              .icons:hover{
                cursor:pointer;
              }
`