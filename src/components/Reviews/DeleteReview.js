import React from 'react';
import { AppContext } from '../../context/AppContext';
import { Modal, Button, Form } from 'react-bootstrap';

export default class DeleteReview extends React.Component {
  render() {
    return (
      <AppContext.Consumer>
        {
          value => {
            const {
              reviewDeleteModal,
              closeReviewModal,
              deleteReview,
              reviewDelete
            } = value
            return (
              <>
                <Modal show={reviewDeleteModal} onHide={closeReviewModal}
                  // aria-labelledby="contained-modal-title-vcenter"
                  centered
                >
                  <Modal.Header closeButton>
                    <Modal.Title>Delete Review</Modal.Title>
                  </Modal.Header>
                  {reviewDelete.map((data, key) => {
                    return (
                      <Modal.Body>
                        <Form key={key}>
                          <Form.Group>
                            <Form.Label>Are you sure you want to delete <span style={{ fontWeight: "bold", textTransform: "uppercase" }}>{data.name}</span> review ?</Form.Label>
                          </Form.Group>
                        </Form>
                        <Modal.Footer>
                          <Button variant="secondary" onClick={closeReviewModal} size="sm">
                            Exit
                          </Button>
                          <Button variant="danger" onClick={() => deleteReview(data._id)} size="sm">
                            Delete
                          </Button>
                        </Modal.Footer>
                      </Modal.Body>
                    )
                  })}
                </Modal>
              </>
            )
          }
        }
      </AppContext.Consumer>
    )
  }
}