import React from 'react';
import styled from 'styled-components';
import { AppContext } from '../../context/AppContext';
import { FaSearch } from 'react-icons/fa';

export default class ViewBar extends React.Component {
  render() {
    return (
      <AppContext.Consumer>
        {
          value => {
            const { test } = value;
            return (
              <MenuWrapper>
                {test}
                <MenuContent>
                  <div className="ml-auto search">
                    <FaSearch style={{ marginLeft: "5px", color: "#a6a6a6" }} /><input type="text" placeholder="Search" size="sm" className="s-text" />
                  </div>
                </MenuContent>
              </MenuWrapper>
            )
          }
        }
      </AppContext.Consumer>
    )
  }
}

const MenuWrapper = styled.div`
  width:84.5%;
  position:fixed;
  top:55px;
  right:0;
  display:flex;
  justify-content:flex-start;
  z-index:2;
  background:#fdfdfd;
  box-shadow:0 2px 2px rgba(0,0,0,0.1);
`
const MenuContent = styled.div`
  width:100%;
  border-radius:5px;
  display:flex;
  flex-wrap:wrap;
  margin:7px 10px;
  .links{
    text-decoration:none;
    color:rgba(0,0,0,0.7);
    font:14px "Times New Roman";
    padding:5px 10px;
    align-content:center;
    display:flex;
    align-items:center;
  }
  .links:hover{
    color:rgb(0,0,0);
  }
  .search {
    display:flex;
    align-items:center;
    text-decoration:none;
  }
  .s-text{
    border:none;
    padding:5px;
    font-size:14px;
    border-radius:2px;
    border-bottom:1px solid rgba(0,0,0,0.1);
  }
  .s-text:focus{
    outline-style:none;
  }
`