import React from 'react';
import styled from 'styled-components';
import bg from '../images/bg.jpg';
import { Container, Form, Button } from 'react-bootstrap';
import { AppContext } from '../context/AppContext';

export default class SignInUp extends React.Component {
  render() {
    return (
      <AppContext.Consumer>
        {
          value => {
            const {
              switches,
              switcher,
              signUp,
              getName,
              getEmail,
              getPassword,
              getKey,
              signIn,
              getLemail,
              getLpassword
            } = value;
            return (
              <SignInUpWrapper>
                <div className="bg"></div>
                <Container>
                  <div className="form-div">
                    <h1>Spice Inn</h1>
                    {switches ?
                      <>
                        <h4 className="span">Login</h4>
                        <Form>
                          <Form.Group>
                            <Form.Label>Email address</Form.Label>
                            <Form.Control size="sm" type="email" placeholder="Enter email" onChange={getLemail} />
                          </Form.Group>
                          <Form.Group>
                            <Form.Label>Password</Form.Label>
                            <Form.Control size="sm" type="password" placeholder="Password" onChange={getLpassword} />
                          </Form.Group>
                          <Button variant="primary" size="sm" onClick={signIn}>
                            Login
                          </Button>
                          <Button
                            variant="link"
                            size="sm"
                            style={{ marginLeft: "10px" }}
                            onClick={switcher}
                          >
                            Sign Up
                          </Button>
                        </Form>
                      </> :
                      <>
                        <h4 className="span">Register</h4>
                        <Form>
                          <Form.Group>
                            <Form.Label>Name</Form.Label>
                            <Form.Control size="sm" type="text" placeholder="Enter name" onChange={getName} />
                          </Form.Group>
                          <Form.Group>
                            <Form.Label>Email address</Form.Label>
                            <Form.Control size="sm" type="email" placeholder="Enter email" onChange={getEmail} />
                          </Form.Group>
                          <Form.Group>
                            <Form.Label>Password</Form.Label>
                            <Form.Control size="sm" type="password" placeholder="Password" onChange={getPassword} />
                          </Form.Group>
                          <Form.Group>
                            <Form.Label>Key</Form.Label>
                            <Form.Control size="sm" type="password" placeholder="Register key" onChange={getKey} />
                          </Form.Group>
                          <Button variant="primary" size="sm" onClick={signUp}>
                            Sign Up
                          </Button>
                          <Form.Text>Already registered
                          <Button
                              variant="link"
                              size="sm"
                              onClick={switcher}
                            >
                              Login
                          </Button>
                          </Form.Text>
                        </Form>
                      </>
                    }
                  </div>
                </Container>
              </SignInUpWrapper>

            )
          }
        }
      </AppContext.Consumer>
    )
  }
}

const SignInUpWrapper = styled.div`
      width:100%;
      height:100vh;
  .bg{
      width:100%;
      height:100vh;
      background:url(${bg});
      background-position:center;
      background-size:cover;
      filter:blur(3px);
      }
  .form-div{
    position:absolute;
    top:50%;
    left:50%;
    width:350px;
    transform:translate(-50%,-50%); 
    background:#f3f3f3;
    padding:10px 20px 20px 20px;
    border-radius:5px;
    box-shadow:0 0 2px 2px rgba(0,0,0,0.3);
    font:14px "Times New Roman";
  }
  .form-div h1{
    color:rgba(0,0,0,0.7);
    font:30px "Times New Roman";
    margin:10px 0;
  }
  .form-div h4{
    background:rgba(0,0,0,0.1);
    margin: 5px 0;
    padding: 5px;
    font-size: 18px;
    font-weight: 600;
    color: rgba(0,0,0,0.5);
  }
`