import React from 'react';
import styled from 'styled-components';
import { AppContext } from '../../context/AppContext';
import { Card } from 'react-bootstrap';
import MenuBar from '../Menus/MenuBar';
import { FaRupeeSign, FaRegTrashAlt, FaRegEdit, FaRegImage } from 'react-icons/fa';
import img from '../../images/bg.jpg';

export default class ViewMenu extends React.Component {
  render() {
    return (
      <AppContext.Consumer>
        {
          value => {
            const { menus, category, handleViewModal, handleImageModal, handleDeleteModal } = value;
            return (
              <>
                <MenuBar />
                <ViewMenuWrapper>
                  <ViewContent>
                    {category.map((data, key) => {
                      return <div>
                        <h1 className="heads" key={key}>{data}</h1>
                        <div className="arrange">
                          {menus.map(menu => {
                            if (menu.labels === data) {
                              return <>
                                <Card className='cards'>
                                  <Card.Img variant="top" src={img} />
                                  <Card.Body>
                                    <Card.Title className="titles">{menu.name} <small>{menu.food_type}</small></Card.Title>
                                    <Card.Title className="span">{menu.description}</Card.Title>
                                    <ul className="uls">
                                      <li className="lists"><FaRupeeSign />{menu.price}</li>
                                      <li className="lists" onClick={() => handleViewModal(menu._id)}><FaRegEdit /></li>
                                      <li className="lists" onClick={() => handleImageModal(menu._id)}><FaRegImage /></li>
                                      <li className="lists" onClick={() => handleDeleteModal(menu._id)}><FaRegTrashAlt /></li>
                                    </ul>
                                  </Card.Body>
                                </Card>
                              </>
                            }
                            return false;
                          })}
                        </div>
                      </div>
                    })}
                  </ViewContent>
                </ViewMenuWrapper>
              </>
            )
          }
        }
      </AppContext.Consumer>
    )
  }
}

const ViewMenuWrapper = styled.div`
  width:85%;
  position:absolute;
  top:100px;
  right:0;
  .heads{
    text-align:left;
    text-transform:capitalize;
    font:bold 25px "Times New Roman";
    padding:10px;
    width:80%;
    border-bottom:1px solid rgba(0,0,0,0.05);
  }
  .titles{
    text-align:left;
    text-transform:capitalize;
    color:#3B3A3A;
    font:bold 16px "Times New Roman";
  }
  .cards{
    border-radius:7px;
    width:220px;
    margin:12px 15px 25px 20px;
  }
  .arrange{
    display:flex;
    flex-wrap:wrap;
    justify-content:felx-start;
  }
  .span{
    text-align:left;
    font:14px "Times New Roman";
  }

  .lists{
    margin: 0 10px;
    list-style-type:none;
  }
  .lists:nth-child(2):hover{
    color:royalblue;
    cursor:pointer;
  }
  .lists:nth-child(3):hover{
    color:royalblue;
    cursor:pointer;
  }
  .lists:nth-child(4):hover{
    color:red;
    cursor:pointer;
  }
  .uls{
    display:flex;
    flex-wrap:wrap;
    justify-content:space-between;
    padding:0;
    margin:0;
  }
`
const ViewContent = styled.div`
  width:90%;
  margin:10px auto;
  box-shadow:0 0 2px 2px rgba(0,0,0,0.1);
  border-radius:5px;
`