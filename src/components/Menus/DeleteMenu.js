import React from 'react';
import { AppContext } from '../../context/AppContext';
import { Modal, Button, Form } from 'react-bootstrap';

export default class DeleteMenu extends React.Component {
  render() {
    return (
      <AppContext.Consumer>
        {
          value => {
            const {
              closeDeleteModal,
              menuDelete,
              deleteMenu,
              menuDeleteModal
            } = value
            return (
              <>
                <Modal show={menuDeleteModal} onHide={closeDeleteModal}
                  // aria-labelledby="contained-modal-title-vcenter"
                  centered
                >
                  <Modal.Header closeButton>
                    <Modal.Title>Delete Menu Item</Modal.Title>
                  </Modal.Header>
                  {menuDelete.map((data, key) => {
                    return (
                      <Modal.Body>
                        <Form key={key}>
                          <Form.Group>
                            <Form.Label>Are you sure you want to delete <span style={{fontWeight:"bold", textTransform:"uppercase"}}>{data.name}</span> menu item ?</Form.Label>
                          </Form.Group>
                        </Form>
                        <Modal.Footer>
                          <Button variant="secondary" onClick={closeDeleteModal} size="sm">
                            Exit
                          </Button>
                          <Button variant="danger" onClick={() => deleteMenu(data._id)} size="sm">
                            Delete
                          </Button>
                        </Modal.Footer>
                      </Modal.Body>
                    )
                  })}
                </Modal>
              </>
            )
          }
        }
      </AppContext.Consumer>
    )
  }
}