import React from 'react';
import { AppContext } from '../../context/AppContext';
import { Modal, Form, Button } from 'react-bootstrap';

export default class EditMenuImage extends React.Component {
  render() {
    return (
      <AppContext.Consumer>
        {
          value => {
            const {
              updateMenuImage,
              closeImageModal,
              getMenuImage,
              imageView,
              menuImgModal
            } = value;
            return (
              <Modal show={menuImgModal} onHide={closeImageModal}>
                <Modal.Header closeButton>
                  <Modal.Title>Update Item Image</Modal.Title>
                </Modal.Header>
                {imageView.map((data, key) => {
                  return (
                    <>
                      <Modal.Body key={key}>
                        {data.imgPath}
                        <img src={'http://localhost:3000/' + data.imgPath} height={200} width={200} alt="item pics" />
                        <Form>
                          <Form.Group>
                            <Form.Label>Select Image</Form.Label>
                            <Form.Control type="file" placeholder="Enter menu name" size="sm" onChange={getMenuImage} />
                          </Form.Group>
                        </Form>
                      </Modal.Body>
                      <Modal.Footer>
                        <Button variant="secondary" onClick={closeImageModal}>
                          Close
                        </Button>
                        <Button variant="primary" onClick={() => updateMenuImage(data._id)}>
                          Update Image
                        </Button>
                      </Modal.Footer>
                    </>
                  )
                })}
              </Modal>
            )
          }
        }
      </AppContext.Consumer>
    )
  }
}