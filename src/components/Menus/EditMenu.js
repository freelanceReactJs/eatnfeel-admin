import React from 'react';
import { AppContext } from '../../context/AppContext';
import { Modal, Button, Form } from 'react-bootstrap';

export default class EditMenu extends React.Component {
  render() {
    return (
      <AppContext.Consumer>
        {
          value => {
            const {
              closeViewModal,
              menuViews,
              menuViewModal,
              getMenuName,
              getMenuDesc,
              getMenuLabel,
              getMenuType,
              getMenuPrice,
              updateMenus
            } = value
            return (
              <>
                <Modal show={menuViewModal} onHide={closeViewModal}
                  // aria-labelledby="contained-modal-title-vcenter"
                  centered
                >
                  <Modal.Header closeButton>
                    <Modal.Title>Update Menu Item</Modal.Title>
                  </Modal.Header>
                  {menuViews.map((data, key) => {
                    return (
                      <Modal.Body>
                        <Form key={key}>
                          <Form.Group>
                            <Form.Label>Item Name</Form.Label>
                            <Form.Control type="text" placeholder="Enter menu name" defaultValue={data.name} size="sm" onChange={getMenuName} />
                          </Form.Group>
                          <Form.Group>
                            <Form.Label>Item Description</Form.Label>
                            <Form.Control type="text" placeholder="Enter menu description" defaultValue={data.description} size="sm" onChange={getMenuDesc} />
                          </Form.Group>
                          <Form.Group>
                            <Form.Label>Item Type</Form.Label>
                            <Form.Control as="select" defaultValue={data.food_type} size="sm" onChange={getMenuType}>
                              <option value="veg">Veg</option>
                              <option value="non-veg">Non-Veg</option>
                            </Form.Control>
                          </Form.Group>
                          <Form.Group>
                            <Form.Label>Item Label</Form.Label>
                            <Form.Control type="text" placeholder="Enter label like (paratha,snacks, etc)" defaultValue={data.labels} size="sm" onChange={getMenuLabel} />
                          </Form.Group>
                          <Form.Group>
                            <Form.Label>Item Price</Form.Label>
                            <Form.Control type="text" placeholder="Enter item price" defaultValue={data.price} size="sm" onChange={getMenuPrice} />
                          </Form.Group>
                        </Form>
                        <Modal.Footer>
                          <Button variant="secondary" onClick={closeViewModal} size="sm">
                            Exit
                          </Button>
                          <Button variant="primary" onClick={() => updateMenus(data._id)} size="sm">
                            Save Changes
                          </Button>
                        </Modal.Footer>
                      </Modal.Body>
                    )
                  })}
                </Modal>
              </>
            )
          }
        }
      </AppContext.Consumer>
    )
  }
}