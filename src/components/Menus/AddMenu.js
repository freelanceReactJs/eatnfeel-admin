import React from 'react';
import { AppContext } from '../../context/AppContext';
import { Button, Form, Row, Col } from 'react-bootstrap';
import styled from 'styled-components';
import MenuBar from '../../components/Menus/MenuBar';

export default class AddMenu extends React.Component {
  render() {
    return (
      <AppContext.Consumer>
        {
          value => {
            const {
              getMenuNameIns,
              getMenuDescIns,
              getMenuLabelIns,
              getMenuTypeIns,
              getMenuPriceIns,
              getMenuImageIns,
              addMenus
            } = value
            return (
              <>
                <MenuBar />
                <FromWrapper>
                  <FormContent>
                    <Form>
                      <h3>Add Menu</h3>
                      <Row>
                        <Col sm={6}>
                          <Form.Group>
                            <Form.Label>Item Name</Form.Label>
                            <Form.Control type="text" placeholder="Enter menu name" size="sm" onChange={getMenuNameIns} />
                          </Form.Group>
                          <Form.Group>
                            <Form.Label>Item Description</Form.Label>
                            <Form.Control type="text" placeholder="Enter menu description" size="sm" onChange={getMenuDescIns} />
                          </Form.Group>
                          <Form.Group>
                            <Form.Label>Item Type</Form.Label>
                            <Form.Control as="select" size="sm" onChange={getMenuTypeIns}>
                              <option value="veg">Veg</option>
                              <option value="non-veg">Non-Veg</option>
                            </Form.Control>
                          </Form.Group>
                        </Col>
                        <Col sm={6}>
                          <Form.Group>
                            <Form.Label>Item Label</Form.Label>
                            <Form.Control type="text" placeholder="Enter label like (paratha,snacks, etc)" size="sm" onChange={getMenuLabelIns} />
                          </Form.Group>
                          <Form.Group>
                            <Form.Label>Item Price</Form.Label>
                            <Form.Control type="text" placeholder="Enter item price" size="sm" onChange={getMenuPriceIns} />
                          </Form.Group>
                          <Form.Group>
                            <Form.Label>Add Image</Form.Label>
                            <Form.Control type="file" size="sm" onChange={getMenuImageIns} />
                          </Form.Group>
                          <Form.Group>
                            <Button variant="secondary" size="sm" style={{ marginRight: "10px" }}>
                              Exit
                          </Button>
                            <Button variant="primary" size="sm" onClick={addMenus}>
                              Insert Menu
                          </Button>
                          </Form.Group>
                        </Col>
                      </Row>
                    </Form>
                  </FormContent>
                </FromWrapper>
              </>
            )
          }
        }
      </AppContext.Consumer>
    )
  }
}

const FromWrapper = styled.div`
  width:85%;
  position:absolute;
  top:50%;
  left:55%;
  transform:translate(-50%,-50%);
  right:0;
  font-family:"Times New Roman";
`
const FormContent = styled.div`
  width:50%;
  padding:10px 20px;
  margin:10px auto;
  border-radius:10px;
  box-shadow:0 0 2px 2px rgba(0,0,0,0.1);
`