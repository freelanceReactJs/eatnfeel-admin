import React from 'react';
import { Navbar, NavDropdown } from 'react-bootstrap';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { AppContext } from '../../context/AppContext';
import { GiCube } from 'react-icons/gi';
import SideMenu from '../Navigations/SideMenu';

export default class TopNavbar extends React.Component {
  render() {
    return (
      <AppContext.Consumer>
        {
          value => {
            const { onLogout } = value;
            return (
              <>
                <NavbarWrapper>
                  <Navbar expand="lg" variant="dark" bg="dark" fixed="top">
                    <Navbar.Brand>
                      <Link to="/" className="brand">
                        <GiCube /> Dashboard
                   </Link>
                    </Navbar.Brand>
                    <NavDropdown title={localStorage.getItem("admin")} id="collasible-nav-dropdown" className="ml-auto fonter">
                      <NavDropdown.Item>
                        <Link to="/settings" className="links">Settings</Link>
                      </NavDropdown.Item>
                      <NavDropdown.Divider />
                      <NavDropdown.Item>
                        <Link to="/" className="links" onClick={onLogout}>Logout</Link>
                      </NavDropdown.Item>
                    </NavDropdown>
                  </Navbar>
                </NavbarWrapper>
                <SideMenu/>
              </>
            )
          }
        }
      </AppContext.Consumer>
    )
  }
}

const NavbarWrapper = styled.div`
.fonter{
  text-transform:capitalize;
}
.fonter a{
  color:#fff;
}
.links{
  color:rgba(0,0,0,0.9) !important;
  text-decoration:none;
}
.brand{
  color:#fff;
  text-decoration:none;
  font:24px "Times New Roman";
}
.brand:hover{
  color:cyan;
}
`