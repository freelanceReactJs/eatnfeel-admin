import React from 'react';
import { AppContext } from '../../context/AppContext';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { IoMdRestaurant, IoIosBook } from "react-icons/io";
import { GiRoundTable } from 'react-icons/gi';
import { FaChalkboardTeacher } from 'react-icons/fa';
import { Badge } from 'react-bootstrap';

export default class SideMenu extends React.Component {
  render() {
    return (
      <AppContext.Consumer>
        {
          value => {
            const { todaysTables } = value;
            return (
              < SideNav >
                <Link to='/order' className="links">
                  <IoMdRestaurant style={{ marginRight: "5px" }} /> Orders
                    <Badge variant="dark" style={{margin: "0 5px auto auto" }}> 10</Badge>
                </Link>
                <Link to='/menu' className="links"><IoIosBook style={{ marginRight: "5px" }} />Menu</Link>
                <Link to='/table' className="links">
                  <GiRoundTable style={{ marginRight: "5px" }} />Table Booking
                    <Badge variant="dark" style={{margin: "0 5px auto auto" }}>{todaysTables.length}</Badge>
                </Link>
                <Link to='/reviews' className="links"><FaChalkboardTeacher style={{ marginRight: "5px" }} />Reviews</Link>
              </SideNav>
            )
          }
        }
      </AppContext.Consumer>
    )
  }
}

const SideNav = styled.div`
position:fixed;
top:56px;
left:0;
  width:15%;
  // background:lavender;
  box-shadow:0 2px 10px rgba(0,0,0,0.3);
  height:100vh;
  .links{
    display:flex;
    flex-wrap:wrap;
    text-decoration:none;
    padding:10px;
    font:16px "Times New Roman";
    color:rgba(0,0,0,0.7);
    box-sizing:border-box;
    border-bottom:1px solid rgba(0,0,0,0.05);
  }
  .links:hover{
    color:#fff;
    background:royalblue;
  }
`