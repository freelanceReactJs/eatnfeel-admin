import React from 'react';
import { AppContext } from '../../context/AppContext';
import styled from 'styled-components';

export default class Dashboard extends React.Component {
  render() {
    return (
      <AppContext.Consumer>
        {
          value => {
            const { lap } = value;
            return (
              <SideBarWrapper>{lap}
                <SideContent>
                  <div className="icons">

                  </div><div className="icons">

                  </div><div className="icons">

                  </div><div className="icons">

                  </div><div className="icons">

                  </div><div className="icons">

                  </div><div className="icons">

                  </div><div className="icons">

                  </div>
                </SideContent>
              </SideBarWrapper>
            )
          }
        }
      </AppContext.Consumer>
    )
  }
}

const SideBarWrapper = styled.div`
width:100%;
`
const SideContent = styled.div`
position:fixed;
top:56px;
right:0;
  width:85%;
  background:#fff;
  height:100vh;
  display:flex;
  flex-wrap:wrap;
  align-content:baseline;
  justify-content:center;
  .icons{
    width:180px;
    height:180px;
    background:#fff;
    margin:20px;
    border-radius:5px;
    box-shadow:0 0 2px 2px rgba(0,0,0,0.1);
  }
`
